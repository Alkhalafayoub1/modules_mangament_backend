package edu.th_owl.modules_management.repository;

import modules_management.entity.Modul;
import modules_management.entity.SemesterType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
class ModulRepoTest {
    @Autowired
    ModulRepo modulRepo;
    // @WithMockUser(username = "admin")
    @Test
    void itShouldFindModulByName() {
        // given
        String modulName = "modul1";
        Modul d = new Modul();
        d.setName(modulName);
        d.setSemesterType(SemesterType.ANY);
        d.setSemesters(Arrays.asList(3, 4));
        d.setCredits(6);
        modulRepo.save(d);
        // when
        Modul model1 = modulRepo.findByName(modulName);
        // then
        assertThat(model1.getName()).isEqualTo(d.getName());
    }

    @Test
    void itShouldNotFoundModulByName() {
        // given
        String modulName = "modul012";
        // when
        Modul model1 = modulRepo.findByName(modulName);
        // then
        assertThat(model1).isNull();
    }

}