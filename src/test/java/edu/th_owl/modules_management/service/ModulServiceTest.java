package edu.th_owl.modules_management.service;

import modules_management.dto.ModulDto;
import modules_management.entity.Department;
import modules_management.entity.Modul;
import modules_management.repository.ModulRepo;
import modules_management.service.ModulService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


@DataMongoTest
class ModulServiceTest {


    static Modul modul;
    static ModulDto modulDto;
    @Autowired
    ModulRepo modulRepo;
    @Autowired
    ModulService modulService;

    @BeforeEach
    void setup() {

        modul = new Modul();
        //  modul.setId(UUID.randomUUID().toString());
        modul.setName("modul1");
        modul.setCredits(6);
        modul.setSemesters(Arrays.asList(1));
        Department department = new Department();
        department.setId(UUID.randomUUID().toString());
        modul.setDepartment(department);
    }

    @AfterEach
    void tearDown() {
        modulRepo.deleteAll();
    }

    //
    @Test
    void save() {
        // given
        modulDto = new ModulDto(modul);
        // when
        ResponseEntity<?> save = modulService.save(modulDto);
        ModulDto modulDto1 = (ModulDto) save.getBody();
        // then
        assertThat(modulDto1.getId()).isNotNull().isNotEmpty();
        assertThat(modulDto.getName()).isEqualTo(modulDto1.getName());
        assertThat(modulDto.getCredits()).isEqualTo(modulDto1.getCredits());
        assertThat(modulDto.getSemesters().get(0)).isEqualTo(modulDto1.getSemesters().get(0));


    }

    @Test
    void getAll() {
        // given

        modulRepo.save(modul);
        // when
        List<ModulDto> all = modulService.getAll();
        // then
        assertThat(all.size()).isEqualTo(1);


    }

    @Test
    void getById() {

// given

        Modul save = modulRepo.save(modul);
        // when
        ModulDto byId = modulService.getById(save.getId());
        // then
        assertThat(byId.getName()).isEqualTo(modul.getName());


    }

    @Test
    void deleteById() {
        // given

        Modul save = modulRepo.save(modul);
        // when
        modulService.deleteById(save.getId());
        // then
        assertThat(modulRepo.findById(save.getId()).isPresent()).isFalse();


    }

    @TestConfiguration
    static class ModulServiceTestContextConfiguration {

        @Bean
        public ModulService modulService() {
            return new ModulService();
        }
    }
}