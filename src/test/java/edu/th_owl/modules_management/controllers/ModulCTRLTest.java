package edu.th_owl.modules_management.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.th_owl.modules_management.dto.ModulDto;
import edu.th_owl.modules_management.entity.Department;
import edu.th_owl.modules_management.entity.Modul;
import edu.th_owl.modules_management.exception.ModulException;
import edu.th_owl.modules_management.service.ModulService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(MockitoExtension.class)
class ModulCTRLTest {

    static Modul modul;
    static ModulDto modulDto;
    @Mock
    ModulService modulService;
    @InjectMocks
    ModulCTRL modulCTRL;
    private MockMvc mvc;

    @BeforeEach
    void setup() {

        JacksonTester.initFields(this, new ObjectMapper());
        // MockMvc standalone approach
        mvc = MockMvcBuilders.standaloneSetup(modulCTRL)
                .setControllerAdvice(new ModulException(""))
                .build();
        modul = new Modul();
        modul.setName("modul1");
        modul.setId("modul1Id");
        modul.setCredits(6);
        modul.setSemesters(Arrays.asList(1));
        Department department = new Department();
        department.setId(UUID.randomUUID().toString());
        modul.setDepartment(department);

    }


    @Test
    public void canRetrieveByIdWhenExists() throws Exception {
        // given
        given(modulService.getAll())
                .willReturn(Arrays.asList(new ModulDto(modul)));

        // when
        MockHttpServletResponse response = mvc.perform(
                get("/api/moduls/all")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        System.out.println(response.getContentAsString());
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

    }


    @Test
    void getSingle() throws Exception {

        // given
        given(modulService.getById(modul.getId()))
                .willReturn(new ModulDto(modul));

        // when
        MockHttpServletResponse response = mvc.perform(
                get("/api/moduls/" + modul.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        System.out.println(response.getContentAsString());
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());


    }

    @Test
    void getAll() throws Exception {
        // given
        given(modulService.getAll())
                .willReturn(Arrays.asList(new ModulDto(modul)));

        // when
        MockHttpServletResponse response = mvc.perform(
                get("/api/moduls/all")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        System.out.println(response.getContentAsString());
        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());


    }

    @Test
    void add() {
        // given

        // when

        // then

    }

    @Test
    void update() {
        // given

        // when

        // then

    }

    @Test
    void delete() {


    }
}