package edu.th_owl.modules_management.entity;

import modules_management.dto.DepartmentDto;
import modules_management.dto.ModulDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class ModulTest {
    static Modul modul;
    static ModulDto modulDto;
    @BeforeAll
    static void beforeAll() {
        modulDto = new ModulDto();
        modulDto.setId(UUID.randomUUID().toString());
        modulDto.setName("modul1");
        modulDto.setCredits(6);
        modulDto.setSemesters(Arrays.asList(1));
        Department department = new Department();
        department.setId(UUID.randomUUID().toString());
        modulDto.setDepartmentDto(new DepartmentDto(department));
    }
    @Test
    void itShouldConvertModulToModulDto() {
        // given
        // when
        modul = new Modul(modulDto);
        // then
        assertThat(modulDto.getId()).isEqualTo(modul.getId());
        assertThat(modulDto.getName()).isEqualTo(modul.getName());
        assertThat(modulDto.getCredits()).isEqualTo(modul.getCredits());
        assertThat(modulDto.getSemesters().get(0)).isEqualTo(modul.getSemesters().get(0));
    }


}