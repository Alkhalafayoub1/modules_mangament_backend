package edu.th_owl.modules_management.dto;

import modules_management.entity.Department;
import modules_management.entity.Modul;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class ModulDtoTest {
    static Modul modul;
    static ModulDto modulDto;


    @BeforeAll
    static void beforeAll() {
        modul = new Modul();
        modul.setId(UUID.randomUUID().toString());
        modul.setName("modul1");
        modul.setCredits(6);
        modul.setSemesters(Arrays.asList(1));
        Department department = new Department();
        department.setId(UUID.randomUUID().toString());
        modul.setDepartment(department);

    }

    @Test
    void itShouldConvertModulToModulDto() {
        // given

        // when
        modulDto = new ModulDto(modul);
        // then
        assertThat(modulDto.getId()).isEqualTo(modul.getId());
        assertThat(modulDto.getName()).isEqualTo(modul.getName());
        assertThat(modulDto.getCredits()).isEqualTo(modul.getCredits());
        assertThat(modulDto.getSemesters().get(0)).isEqualTo(modul.getSemesters().get(0));


    }

    @Test
    void checkSelfStudyAndWorkLoad() {
        // given

        // when
        modulDto = new ModulDto(modul);
        // then
        assertThat(modulDto.getSelfStudy()).isEqualTo(120);
        assertThat(modulDto.getWorkLoad()).isEqualTo(180);

    }


}