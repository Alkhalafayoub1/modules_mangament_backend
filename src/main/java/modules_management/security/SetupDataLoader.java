package modules_management.security;

import modules_management.entity.ERole;
import modules_management.entity.Role;
import modules_management.entity.User;
import modules_management.repository.RoleRepo;
import modules_management.repository.UserRepo;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;
    @Autowired
    UserRepo userRepo;
    @Autowired
    RoleRepo roleRepo;
    @Autowired
    PasswordEncoder encoder;

    @SneakyThrows
    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup)  return;

createRoleIfNotExist(ERole.ROLE_ADMIN);
createRoleIfNotExist(ERole.ROLE_PROFESSOR);
createRoleIfNotExist(ERole.ROLE_USER);

        createUserIfNotExist();


        alreadySetup = true;
    }

    @Transactional
   void createUserIfNotExist(){
        if(!userRepo.findByEmail("admin").isPresent()) {
            User user = new User(
                    "admin",
                    encoder.encode("admin"));
            user.setFirstName("admin");
            user.setLastName("admin");
            user.setEnabled(true);
            Set<Role> roles = new HashSet<>();
            Role adminRole = roleRepo.findByName("ROLE_ADMIN")
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);
            user.setRoles(roles);

            userRepo.save(user);
        }



    }
    @Transactional
    void createRoleIfNotExist(ERole eRole){
if(!roleRepo.findByName(eRole.name()).isPresent())
        roleRepo.save(new Role(eRole));
    }



}