package modules_management.dto;

import modules_management.entity.Department;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DepartmentDto {

    String id;
    String name;
    public DepartmentDto(Department department) {
        if(department!=null){
        this.id = department.getId();
        this.name = department.getName();}
    }

    @Override
    public String toString() {
        return "DepartmentDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
