package modules_management.dto;

import modules_management.entity.ModulType;
import modules_management.entity.SubSubDepData;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubSubDepDataDto {
    SubSubDepartmentDto subSubDepartment;
    ModulType modulType;

    public SubSubDepDataDto(SubSubDepData subSubDepData) {
        this.subSubDepartment = new SubSubDepartmentDto(subSubDepData.getSubSubDepartment());
        this.modulType = subSubDepData.getModulType();
    }
}
