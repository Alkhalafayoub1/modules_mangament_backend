package modules_management.dto;

import modules_management.entity.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class BaseDto {

    String id;
    @NotBlank
    @NotNull
    String name;

    public BaseDto(BaseEntity baseEntity) {
        this.id = baseEntity.getId();
        this.name = baseEntity.getName();
    }
}
