package modules_management.dto;

import modules_management.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class UserDto {
    String id, firstName, lastName, email;

    private Set<String> roles = new HashSet<>();
    private boolean enabled;

    public UserDto(User user) {
        if (user != null) {
            this.id = user.getId();
            this.firstName = user.getFirstName();
            this.lastName = user.getLastName();
            this.email = user.getEmail();
            user.getRoles().forEach(role -> roles.add(role.getName().name()));
            this.enabled = user.isEnabled();
        }
    }


}
