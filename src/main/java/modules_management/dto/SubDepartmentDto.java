package modules_management.dto;

import modules_management.entity.SubDepartment;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class SubDepartmentDto  {

    String id;
    @NotBlank
    @NotNull
    String name;

    private DepartmentDto departmentDto;


    public SubDepartmentDto(SubDepartment subDepartment) {
        if(subDepartment!=null){
        this.id = subDepartment.getId();
        this.name = subDepartment.getName();
        this.departmentDto = new DepartmentDto(subDepartment.getDepartment());}
    }

    @Override
    public String toString() {
        return "SubDepartmentDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", departmentDto=" + departmentDto +
                '}';
    }
}
