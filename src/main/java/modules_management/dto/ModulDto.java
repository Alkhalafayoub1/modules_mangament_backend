package modules_management.dto;

import modules_management.entity.Modul;
import modules_management.entity.SemesterType;
import modules_management.entity.StudyData;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ModulDto {

    String id;
    @NotBlank
    @NotNull
    String name;
    DepartmentDto departmentDto;
    SubDepartmentDto subDepartment;
    private List<SubSubDepDataDto> subsubdepartments =new ArrayList<>();
    private String shortcut;
    private SemesterType semesterType;
    private double duration;
    private CategoryDto category;
    private List<StudyData> studyData = new ArrayList<>();
    private List<Integer> semesters = new ArrayList<>();
    private List<UserDto> managers = new ArrayList<>();
    private int modulno, credits,
            selfStudy,
            workload;

    public ModulDto(Modul modul) {
        this.id = modul.getId();
        this.name = modul.getName();
        this.shortcut = modul.getShortcut();
        this.semesterType = modul.getSemesterType();
        this.duration = modul.getDuration();
        this.studyData = modul.getStudyData();
        this.semesters = modul.getSemesters();
        this.modulno = modul.getModulno();
        this.credits = modul.getCredits();
        if (modul.getCategory() != null)this.category=new CategoryDto(modul.getCategory());
        if (modul.getDepartment() != null)
            this.departmentDto = new DepartmentDto(modul.getDepartment());
        if (modul.getSubDepartment() != null&&modul.getSubDepartment().getId() != null)
            this.subDepartment = new SubDepartmentDto(modul.getSubDepartment());
        modul.getSubsubdepartments().forEach(s->this.subsubdepartments.add(new SubSubDepDataDto(s)));
        if (modul.getManagers() != null)
            modul.getManagers().forEach(m->this.managers.add(new UserDto(m)));

        getSelfStudy();
        getWorkLoad();
    }

    public int getSelfStudy() {
        int swsSum = 4;
        for (StudyData studyData : this.studyData)
            swsSum += studyData.getSws();
        return getWorkLoad() - (15 * swsSum);
    }

    public int getWorkLoad() {
        return this.credits * 30;
    }


}
