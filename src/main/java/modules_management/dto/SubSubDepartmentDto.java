package modules_management.dto;

import modules_management.entity.SubSubDepartment;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubSubDepartmentDto extends BaseDto {

    SubDepartmentDto subDepartmentDto;

    public SubSubDepartmentDto(SubSubDepartment subSubDep) {
        this.id = subSubDep.getId();
        this.name = subSubDep.getName();
        this.subDepartmentDto = new SubDepartmentDto(subSubDep.getSubDepartment());

    }
}
