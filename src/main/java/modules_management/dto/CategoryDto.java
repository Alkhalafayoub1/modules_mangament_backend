package modules_management.dto;


import modules_management.entity.Category;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CategoryDto extends BaseDto {


    UserDto manager;

    public CategoryDto(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.manager = new UserDto(category.getManager());
    }


}
