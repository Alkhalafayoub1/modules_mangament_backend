package modules_management.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatisticDto {
    String name;
    int count;

  public   void increaseCount(int i){
        this.count+=i;
    }

}
