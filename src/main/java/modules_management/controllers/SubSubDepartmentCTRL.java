package modules_management.controllers;


import modules_management.dto.SubSubDepartmentDto;
import modules_management.service.SubSubDepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/subsubdepartments")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@CrossOrigin(origins = "*", maxAge = 3600)

public class SubSubDepartmentCTRL {


    @Autowired
    SubSubDepartmentService subSubDepartmentService;

    @GetMapping("{id}")
    public SubSubDepartmentDto getSingle(@PathVariable("id") String id) {
        return subSubDepartmentService.getById(id);
    }

    @GetMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public List<SubSubDepartmentDto> getAll() {
        return subSubDepartmentService.getAll();
    }
    @GetMapping("subDepartment")
    @ResponseStatus(HttpStatus.OK)
    public List<SubSubDepartmentDto> getBySubDepartment(@RequestParam("subDepartmentId")String subDepartmentId) {
        return subSubDepartmentService.getAll();
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<SubSubDepartmentDto> getPage(@RequestParam(value = "page", defaultValue = "1") int page,
                                          @RequestParam(value = "size", defaultValue = "20") int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return subSubDepartmentService.getPage(pageable);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> add(@RequestBody SubSubDepartmentDto SubSubDepartmentDto) {

        return subSubDepartmentService.save(SubSubDepartmentDto);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> update(@RequestBody SubSubDepartmentDto SubSubDepartmentDto) {
        return subSubDepartmentService.save(SubSubDepartmentDto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") String id) {
        subSubDepartmentService.deleteById(id);
    }

}
