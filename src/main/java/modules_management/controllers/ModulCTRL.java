package modules_management.controllers;


import modules_management.dto.ModulDto;
import modules_management.service.ModulService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/moduls")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ModulCTRL {


    @Autowired
    ModulService modulService;

    @GetMapping("{id}")
    public ModulDto getSingle(@PathVariable("id") String id) {
        return modulService.getById(id);
    }

    @GetMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public List<ModulDto> getAll() {
        return modulService.getAll();
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<ModulDto> getPage(@RequestParam(value = "page", defaultValue = "1") int page,
                                       @RequestParam(value = "size", defaultValue = "20") int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return modulService.getPage(pageable);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> add(@RequestBody ModulDto modulDto) {
System.out.println(modulDto);
        return modulService.save(modulDto);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> update(@RequestBody ModulDto modulDto) {
        return modulService.update(modulDto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") String id) {
        modulService.deleteById(id);
    }


    @GetMapping("checkModulName/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean checkModulName(@PathVariable("name") String name) {
        return true;
    }

    @GetMapping("checkModulNumber/{number}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean checkModulNumber(@PathVariable("number") String number) {
        return true;
    }

    @GetMapping("checkModulShortcut/{shortcut}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean checkModulShortcut(@PathVariable("shortcut") String shortcut) {
        return true;
    }

    @GetMapping("history")
    @ResponseStatus(HttpStatus.OK)
    public List<ModulDto> getHistory() {
        return modulService.getAll();
    }



}
