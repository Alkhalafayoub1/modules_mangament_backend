package modules_management.controllers;


import modules_management.dto.CategoryDto;
import modules_management.exception.CategoryException;
import modules_management.security.services.UserDetailsServiceImpl;
import modules_management.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/categories")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class CategoryCTRL {
    @Autowired
    CategoryService categoryService;
    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @GetMapping("{id}")
    public CategoryDto getSingle(@PathVariable("id") String id) throws CategoryException {
        return categoryService.getById(id);
    }
    @GetMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryDto> getAll() {
        return categoryService.getAll();
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<CategoryDto> getPage(@RequestParam(value = "page", defaultValue = "1") int page,
                                    @RequestParam(value = "size", defaultValue = "20") int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return categoryService.getPage(pageable);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> add(@RequestBody CategoryDto categoryDto) {
        return categoryService.save(categoryDto);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> update(@RequestBody CategoryDto categoryDto) {
        return categoryService.save(categoryDto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean delete(@PathVariable String id) {
        categoryService.deleteById(id);
        return true;
    }


}
