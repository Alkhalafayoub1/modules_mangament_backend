package modules_management.controllers;


import modules_management.service.PDFService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_PDF;


@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/reports/")
public class PDFController {
@Autowired
    PDFService pdfService;
    // generate  pdf
    @RequestMapping(value = "/moduls", produces = "application/pdf")
    public ResponseEntity<InputStreamResource> modulsPDF(@RequestParam(value = "userId", defaultValue = "") String userid) throws IOException {


        log.info("Start invoice generation...");

        final File invoicePdf = pdfService.generateInvoiceFor();
        log.info("Invoice generated successfully...");

        final HttpHeaders httpHeaders = getHttpHeaders( invoicePdf);
        return new ResponseEntity<>(new InputStreamResource(new FileInputStream(invoicePdf)), httpHeaders, OK);
    }

    private HttpHeaders getHttpHeaders( File invoicePdf) {
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(APPLICATION_PDF);
        respHeaders.setContentLength(invoicePdf.length());
        respHeaders.setContentDispositionFormData("attachment", "Moduls.pdf");
        return respHeaders;
    }



}
