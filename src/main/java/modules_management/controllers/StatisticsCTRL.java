package modules_management.controllers;

import modules_management.dto.StatisticDto;
import modules_management.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/statistics")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class StatisticsCTRL {
    @Autowired
    StatisticsService statisticsService;


    @GetMapping("moduls")
    long getModulCount() {
        return statisticsService.getModulCount();
    }
    @GetMapping("users")
    long getUserCount() {
        return statisticsService.getUserCount();
    }
    @GetMapping("sws")
    long getSWS() {
        return statisticsService.getSWS();
    }
    @GetMapping("studytypes")
    List<StatisticDto> getStudyTypeStatistics() {
        return statisticsService.getStudyTypeStatistics();
    }
    @GetMapping("workloads")
    List<StatisticDto> getUsersWorkloads() {
        return statisticsService.getUsersWorkloads();
    }


}
