package modules_management.controllers;


import modules_management.dto.UserDto;
import modules_management.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/users")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class UserCTRL {
    @Autowired
    UserService userService;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAll()  {

        List<UserDto> userDtos = userService.getAll();
        System.out.println("userDtos: " + userDtos);
        return userDtos;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto add(@RequestBody UserDto userDto) throws IOException {

        ResponseEntity<?> save = userService.save(userDto);
        System.out.println("ResponseEntity: " + save);
        return userDto;
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<UserDto> getPage(@RequestParam(value = "page", defaultValue = "1") int page,
                                    @RequestParam(value = "size", defaultValue = "21") int size) throws IOException {




        Pageable pageable = PageRequest.of(page - 1, size);
        return userService.getPage(pageable);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody UserDto userDto){

        return userService.update(userDto);
    }


}
