package modules_management.controllers;


import modules_management.dto.BaseDto;
import modules_management.exception.StudyTypeException;
import modules_management.service.StudyTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/studytypes")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@CrossOrigin(origins = "*", maxAge = 3600)

public class StudyTypeCTRL {
    @Autowired
    StudyTypeService studyTypeService;

    @GetMapping("{id}")
    public BaseDto getSingle(@PathVariable("id") String id) throws StudyTypeException {
        return studyTypeService.getById(id);
    }

    @GetMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public List<BaseDto> getAll() {
        return studyTypeService.getAll();
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<BaseDto> getPage(@RequestParam(value = "page", defaultValue = "1") int page,
                                       @RequestParam(value = "size", defaultValue = "20") int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return studyTypeService.getPage(pageable);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> add(@RequestBody BaseDto studyTypeDto) {

        return studyTypeService.save(studyTypeDto);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> update(@RequestBody BaseDto studyTypeDto) {
        return studyTypeService.save(studyTypeDto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") String id) {
        studyTypeService.deleteById(id);
    }

}
