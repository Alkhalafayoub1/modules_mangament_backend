package modules_management.controllers;


import modules_management.dto.DepartmentDto;
import modules_management.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/departments")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DepartmentCTRL {


    @Autowired
    DepartmentService departmentService;

    @GetMapping("{id}")
    public DepartmentDto getSingle(@PathVariable("id") String id) {
        return departmentService.getById(id);
    }

    @GetMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public List<DepartmentDto> getAll() {
        return departmentService.getAll();
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<DepartmentDto> getPage(@RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "20") int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return departmentService.getPage(pageable);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> add(@RequestBody DepartmentDto DepartmentDto) {

        return departmentService.save(DepartmentDto);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> update(@RequestBody DepartmentDto DepartmentDto) {
        return departmentService.save(DepartmentDto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") String id) {
        departmentService.deleteById(id);
    }

}
