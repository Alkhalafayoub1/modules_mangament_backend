package modules_management.controllers;


import modules_management.dto.SubDepartmentDto;
import modules_management.service.SubDepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/subdepartments")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@CrossOrigin(origins = "*", maxAge = 3600)

public class SubDepartmentCTRL {


    @Autowired
    SubDepartmentService subDepartmentService;

    @GetMapping("{id}")
    public SubDepartmentDto getSingle(@PathVariable("id") String id) {
        return subDepartmentService.getById(id);
    }

    @GetMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public List<SubDepartmentDto> getAll() {
        return subDepartmentService.getAll();
    }


    @GetMapping("/department")
    @ResponseStatus(HttpStatus.OK)
    public List<SubDepartmentDto> getByDepId(@RequestParam("departmentId")String depId) {
        System.out.println("depId:"+depId);
        return subDepartmentService.getAll();
    }



    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<SubDepartmentDto> getPage(@RequestParam(value = "page", defaultValue = "1") int page,
                                       @RequestParam(value = "size", defaultValue = "20") int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return subDepartmentService.getPage(pageable);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> add(@RequestBody SubDepartmentDto subDepartmentDto) {
System.out.println(subDepartmentDto.toString());
        return subDepartmentService.save(subDepartmentDto);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> update(@RequestBody SubDepartmentDto subDepartmentDto) {
        return subDepartmentService.save(subDepartmentDto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") String id) {
        subDepartmentService.deleteById(id);
    }

}
