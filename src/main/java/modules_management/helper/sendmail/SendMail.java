package modules_management.helper.sendmail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@Service
public class SendMail {
    @Autowired
    private JavaMailSender javaMailSender;

    @Async
    public void sendEmailWithAttachment(String receiver, String path) throws IOException {
        try {
            MimeMessage msg = javaMailSender.createMimeMessage();

            // true = multipart message
            MimeMessageHelper helper = new MimeMessageHelper(msg, false);
            helper.setTo(receiver);
            helper.setSubject("TH WOL Account");

            //  path = "http://berfin-env.eba-bhm3uzva.eu-west-3.elasticbeanstalk.com/" + path;
            path = "http://localhost:8082/" + path;
            helper.setText(getmail(receiver, path), true);
            System.out.println("send ....");

            javaMailSender.send(msg);
        } catch (MessagingException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("send done!");
    }

    public void sendMail() {

        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setTo("faragmikhail29@gmail.com");
        mailMessage.setSubject("dddddddddddddd");
        mailMessage.setText("ssssssssssssssssssssssssss");

        mailMessage.setFrom("johndoe@example.com");

        javaMailSender.send(mailMessage);
    }


    String getmail(String username, String password) {
        return "\n" +
                "<!doctype html>\n" +
                "<html lang=\"en-US\">\n" +
                "\n" +
                "<head>\n" +
                "    <meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />\n" +
                "    <title>TH WOL Password </title>\n" +
                "    <meta name=\"description\" content=\"Reset Password .\">\n" +
                "    <style type=\"text/css\">\n" +
                "        a:hover {text-decoration: underline !important;}\n" +
                "    </style>\n" +
                "</head>\n" +
                "\n" +
                "<body marginheight=\"0\" topmargin=\"0\" marginwidth=\"0\" style=\"margin: 0px; background-color: #f2f3f8;\" leftmargin=\"0\">\n" +
                " <h5> username : " + username + " </h5>\n" +
                " <h5> password : " + password + " </h5>\n" +

                "  <!--100% body table-->\n" +


                "</body>\n" +
                "\n" +
                "</html>";
    }




}
