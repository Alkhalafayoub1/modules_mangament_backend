package modules_management.exception;

public class StudyTypeException extends TypeNotPresentException {

    public StudyTypeException(String msg) {
        super(msg, new Throwable());
    }


}
