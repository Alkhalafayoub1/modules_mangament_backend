package modules_management.exception;

public class RoleException extends TypeNotPresentException {

    public RoleException(String msg) {
        super(msg, new Throwable());
    }


}
