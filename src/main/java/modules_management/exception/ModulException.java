package modules_management.exception;

public class ModulException extends TypeNotPresentException {

    public ModulException(String msg) {
        super(msg, new Throwable());
    }


}
