package modules_management.exception;

public class DepartmentException extends TypeNotPresentException {

    public DepartmentException(String msg) {
        super(msg, new Throwable());
    }


}
