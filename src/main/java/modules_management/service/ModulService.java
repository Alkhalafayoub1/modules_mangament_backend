package modules_management.service;

import modules_management.dto.ModulDto;
import modules_management.entity.Modul;
import modules_management.exception.ModulException;
import modules_management.repository.ModulRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ModulService {

    @Autowired
    ModulRepo modulRepo;

    public ResponseEntity<?> save(ModulDto modulDto) {
        if (modulDto.getName() == null || modulDto.getName().isEmpty())
            return new ResponseEntity<>("Modul must have name at least", HttpStatus.BAD_REQUEST);
        if (modulRepo.findByName(modulDto.getName()) != null && modulDto.getId() == null)
            return new ResponseEntity<>("Modul already exist..", HttpStatus.IM_USED);
        Modul modul = new Modul(modulDto);
       modul.setEnabled(true);
        Modul save = modulRepo.save(modul);
        ModulDto modulDto1 = new ModulDto(save);
        log.info("\n new {}\n added by {}", save, modulDto1);
        return new ResponseEntity<>(modulDto1, HttpStatus.CREATED);

    }
    public ResponseEntity<?> update(ModulDto modulDto) {
        if (modulDto.getName() == null || modulDto.getName().isEmpty())
            return new ResponseEntity<>("Modul must have name at least", HttpStatus.BAD_REQUEST);

        Modul modul = new Modul(modulDto);

        Modul save = modulRepo.save(modul);
        ModulDto modulDto1 = new ModulDto(save);
        log.info("\n new {}\n update by {}", save, modulDto1);
        return new ResponseEntity<>(modulDto1, HttpStatus.CREATED);

    }

    public List<ModulDto> getAll() {
        return modulRepo.findAll().stream().map(ModulDto::new).collect(Collectors.toList());
    }

    public ModulDto getById(String id) {
        return new ModulDto(Objects.requireNonNull(modulRepo.findById(id).orElseThrow(() -> new ModulException("Modul Not Found"))));
    }


    public void deleteById(String id) {
        modulRepo.deleteById(id);
    }

    public Page<ModulDto> getPage(Pageable pageable) {
        Page<Modul> entities = modulRepo.findAll(pageable);
        Page<ModulDto> dtoPage = entities.map(new Function<Modul, ModulDto>() {
            @Override
            public ModulDto apply(Modul entity) {
                ModulDto dto = new ModulDto(entity);
                // Conversion logic
                return dto;
            }
        });
        return dtoPage;
    }

}
