package modules_management.service;

import modules_management.dto.CategoryDto;
import modules_management.entity.Category;
import modules_management.exception.CategoryException;
import modules_management.repository.CategoryRepo;
import modules_management.security.services.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CategoryService {
    @Autowired
    CategoryRepo categoryRepo;
    @Autowired
    UserDetailsServiceImpl userDetailsService;

    public ResponseEntity<?> save(CategoryDto categoryDto) {
        if (categoryDto.getName() == null || categoryDto.getName().isEmpty())
            return new ResponseEntity<>("Category must have name at least", HttpStatus.BAD_REQUEST);
        if (categoryRepo.findByName(categoryDto.getName()) != null && categoryDto.getId() == null)
            return new ResponseEntity<>("Category already exist..", HttpStatus.IM_USED);
        Category category = new Category(categoryDto);
//        if(category.getId().isEmpty())category.setId(null);
         log.info("\n new {} added by {}", category.toString(), userDetailsService.getCurrentUser().getUsername());
        Category save = categoryRepo.save(category);
       return new ResponseEntity<>(new CategoryDto(Objects.requireNonNull(categoryRepo.findById(save.getId()).orElse(null))), HttpStatus.CREATED);
    }

    public ResponseEntity<?> update(CategoryDto categoryDto) {
        if (categoryDto.getName() == null || categoryDto.getName().isEmpty())
            return new ResponseEntity<>("Category must have name at least", HttpStatus.BAD_REQUEST);
        Category category = new Category(categoryDto);
            log.info("\n {} updated to {}\n by {}", categoryRepo.findById(categoryDto.getId()).get().toString(), category.toString(), userDetailsService.getCurrentUser().getUsername());
        Category save = categoryRepo.save(category);
        return new ResponseEntity<>(new CategoryDto(Objects.requireNonNull(categoryRepo.findById(save.getId()).orElse(null))), HttpStatus.CREATED);
    }


    public List<CategoryDto> getAll( ) {
        return categoryRepo.findAll( ).stream().map(CategoryDto::new).collect(Collectors.toList());
    }

    public Page<CategoryDto> getPage(Pageable pageable) {
        // return categoryRepo.findAll( pageable).stream().map(CategoryDto::new).collect(Collectors.toList());
        Page<Category> entities = categoryRepo.findAll(pageable);
        Page<CategoryDto> dtoPage = entities.map(new Function<Category, CategoryDto>() {
            @Override
            public CategoryDto apply(Category entity) {
                CategoryDto dto = new CategoryDto(entity);
                // Conversion logic

                return dto;
            }
        });

        return dtoPage;

    }

    public CategoryDto getById(String id) {
        return new CategoryDto(Objects.requireNonNull(categoryRepo.findById(id).orElseThrow(() -> new CategoryException("Category Not Found"))));
    }


    public void deleteById(String categoryId) {
        categoryRepo.deleteById(categoryId);
    }
}
