package modules_management.service;

import modules_management.dto.BaseDto;
import modules_management.entity.StudyType;
import modules_management.exception.StudyTypeException;
import modules_management.repository.StudyTypeRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StudyTypeService {
    @Autowired
    StudyTypeRepo studyTypeRepo;

    public ResponseEntity<?> save(BaseDto studyTypeDto) {
        if (studyTypeDto.getName() == null || studyTypeDto.getName().isEmpty())
            return new ResponseEntity<>("StudyType must have name at least", HttpStatus.BAD_REQUEST);
        if (studyTypeRepo.findByName(studyTypeDto.getName()) != null && studyTypeDto.getId() == null)
            return new ResponseEntity<>("StudyType already exist..", HttpStatus.IM_USED);
        StudyType studyType = new StudyType(studyTypeDto);
        if (studyType.getId() != null)
            log.info("\n {}\n updated to {}\n by {}", studyTypeRepo.findById(studyTypeDto.getId()).get(), studyType, studyTypeDto);
        else log.info("\n new {}\n added by {}", studyType, studyTypeDto);
        StudyType save = studyTypeRepo.save(studyType);
        return new ResponseEntity<>(new BaseDto(Objects.requireNonNull(studyTypeRepo.findById(save.getId()).orElse(null))), HttpStatus.CREATED);

    }

    public List<BaseDto> getAll() {
        return studyTypeRepo.findAll().stream().map(BaseDto::new).collect(Collectors.toList());
    }

    public BaseDto getById(String id) {
        return new BaseDto(Objects.requireNonNull(studyTypeRepo.findById(id).orElseThrow(() -> new StudyTypeException("StudyType Not Found"))));
    }


    public void deleteById(String studyTypeId) {
        studyTypeRepo.deleteById(studyTypeId);
    }

    public Page<BaseDto> getPage(Pageable pageable) {
        Page<StudyType> entities = studyTypeRepo.findAll(pageable);
        Page<BaseDto> dtoPage = entities.map(new Function<StudyType, BaseDto>() {
            @Override
            public BaseDto apply(StudyType entity) {
                BaseDto dto = new BaseDto(entity);
                // Conversion logic
                return dto;
            }
        });
        return dtoPage;
    }
}
