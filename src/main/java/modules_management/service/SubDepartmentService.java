package modules_management.service;

import modules_management.dto.SubDepartmentDto;
import modules_management.entity.SubDepartment;
import modules_management.exception.DepartmentException;
import modules_management.repository.SubDepartmentRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SubDepartmentService {

    @Autowired
    SubDepartmentRepo subDepartmentRepo;

    public ResponseEntity<?> save(SubDepartmentDto subDepartmentDto) {
        if (subDepartmentDto.getName() == null || subDepartmentDto.getName().isEmpty())
            return new ResponseEntity<>("Department must have name at least", HttpStatus.BAD_REQUEST);
        if (subDepartmentRepo.findByName(subDepartmentDto.getName()) != null && subDepartmentDto.getId() == null)
            return new ResponseEntity<>("Department already exist..", HttpStatus.IM_USED);
        SubDepartment subDepartment = new SubDepartment(subDepartmentDto);
        if (subDepartment.getId() != null)
            log.info("\n {}\n updated to {}\n by {}", subDepartmentRepo.findById(subDepartmentDto.getId()).get(), subDepartment, subDepartmentDto);
        else log.info("\n new {}\n added by {}", subDepartment, subDepartmentDto);
        SubDepartment save = subDepartmentRepo.save(subDepartment);
        return new ResponseEntity<>(new SubDepartmentDto(Objects.requireNonNull(subDepartmentRepo.findById(save.getId()).orElse(null))), HttpStatus.CREATED);

    }

    public List<SubDepartmentDto> getAll() {
        return subDepartmentRepo.findAll().stream().map(SubDepartmentDto::new).collect(Collectors.toList());
    }

    public SubDepartmentDto getById(String id) {
        return new SubDepartmentDto(Objects.requireNonNull(subDepartmentRepo.findById(id).orElseThrow(() -> new DepartmentException("Department Not Found"))));
    }


    public void deleteById(String id) {
        subDepartmentRepo.deleteById(id);
    }
    public Page<SubDepartmentDto> getPage(Pageable pageable) {
        Page<SubDepartment> entities = subDepartmentRepo.findAll(pageable);
        Page<SubDepartmentDto> dtoPage = entities.map(new Function<SubDepartment, SubDepartmentDto>() {
            @Override
            public SubDepartmentDto apply(SubDepartment entity) {
                SubDepartmentDto dto = new SubDepartmentDto(entity);
                // Conversion logic
                return dto;
            }
        });
        return dtoPage;
    }

}
