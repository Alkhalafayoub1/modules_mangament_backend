package modules_management.service;

import modules_management.dto.UserDto;
import modules_management.entity.User;
import modules_management.exception.UserException;
import modules_management.helper.sendmail.SendMail;
import modules_management.repository.RoleRepo;
import modules_management.repository.UserRepo;
import modules_management.security.services.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserService {
    @Autowired
    UserRepo userRepo;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    RoleRepo roleRepo;
    @Autowired
    UserDetailsServiceImpl userDetailsService;
    @Autowired
    SendMail sendMail;


    public ResponseEntity<?> save(UserDto userDto) throws IOException {
        if (userRepo.findByEmail(userDto.getEmail()).isPresent() )
            return new ResponseEntity<>("User already exist..", HttpStatus.IM_USED);
        User user = new User(userDto);
        log.info("\n new {} added by {}", user.toString(), userDetailsService.getCurrentUser().getUsername());
        user.setPassword(encoder.encode("123456"));
        user.getRoles().add(roleRepo.findByName(userDto.getRoles().iterator().next()).get());
        user.setEnabled(true);
        User save = userRepo.save(user);
    //    sendMail.sendEmailWithAttachment("faragmikhail29@gmail.com","from ayoub project");


        return new ResponseEntity<>(new UserDto(save), HttpStatus.CREATED);
    }

    public ResponseEntity<?> update(UserDto userDto) {
        if (userDto.getEmail() == null || userDto.getEmail().isEmpty())
            return new ResponseEntity<>("User must have name at least", HttpStatus.BAD_REQUEST);
        User user = new User(userDto);
        log.info("\n {} updated to {}\n by {}", userRepo.findById(userDto.getId()).get().toString(), user.toString(), userDetailsService.getCurrentUser().getUsername());
       user.setPassword(userRepo.findById(user.getId()).get().getPassword());
        user.getRoles().add(roleRepo.findByName(userDto.getRoles().iterator().next()).get());
        User save = userRepo.save(user);
        return new ResponseEntity<>(new UserDto(save), HttpStatus.CREATED);
    }
    public List<UserDto> getAll() {
        return userRepo.findAll().stream().map(UserDto::new).collect(Collectors.toList());
    }
    public Page<UserDto> getPage(Pageable pageable) {
        // return categoryRepo.findAll( pageable).stream().map(CategoryDto::new).collect(Collectors.toList());
        Page<User> entities = userRepo.findAll(pageable);
        Page<UserDto> dtoPage = entities.map(new Function<User, UserDto>() {
            @Override
            public UserDto apply(User entity) {
                UserDto dto = new UserDto(entity);
                return dto;
            }
        });
        return dtoPage;

    }

    public List<UserDto> getAllEnabledUsers() {
        return userRepo.findByEnabled(true).stream().map(UserDto::new).collect(Collectors.toList());
    }

    public UserDto getUserById(String id) throws UserException {
        return new UserDto(userRepo.findById(id).orElseThrow(() -> new UserException("User not found")));
    }


}
