package modules_management.service;


import modules_management.entity.Modul;
import modules_management.repository.ModulRepo;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.jasperreports.JasperReportsUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class PDFService {

@Autowired
ModulRepo modulRepo;


    @Value("${PDF.logo.path}")
    private String logo_path;


    @Value("${adminPDF.template.path}")
    private String modul_template;

    public File generateInvoiceFor() throws IOException {
        File pdfFile = File.createTempFile("my-moduls", ".pdf");
        log.info(String.format(" pdf path : %s", pdfFile.getAbsolutePath()));
        try (FileOutputStream pos = new FileOutputStream(pdfFile)) {
            final JasperReport report = loadTemplate();
            final Map<String, Object> parameters = new HashMap<>();
            parameters.put("logo", getClass().getResourceAsStream(logo_path));
            List<Modul> moduls = modulRepo.findAll();
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(moduls);
            JasperReportsUtils.renderAsPdf(report, parameters, dataSource, pos);
        } catch (final Exception e) {
            log.error(String.format("An error occured during PDF creation: %s", e));
            throw new RuntimeException(e);
        }
        return pdfFile;
    }



    // Load  JRXML template
    private JasperReport loadTemplate() throws JRException {
        log.info(String.format("PDF template path : %s", modul_template));
        final InputStream reportInputStream = getClass().getResourceAsStream(modul_template);
        final JasperDesign jasperDesign = JRXmlLoader.load(reportInputStream);
        return JasperCompileManager.compileReport(jasperDesign);
    }

}
