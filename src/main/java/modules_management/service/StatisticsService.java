package modules_management.service;


import modules_management.dto.StatisticDto;
import modules_management.entity.Modul;
import modules_management.entity.StudyData;
import modules_management.entity.StudyType;
import modules_management.entity.User;
import modules_management.repository.ModulRepo;
import modules_management.repository.StudyTypeRepo;
import modules_management.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StatisticsService {
    @Autowired
    ModulRepo modulRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    StudyTypeRepo studyTypeRepo;

    public long getModulCount() {
        return modulRepo.countByEnabled(true);

    }

    public long getUserCount() {

        return userRepo.countByEnabled(true);

    }

    public long getSWS() {
        List<Modul> moduls = modulRepo.findByEnabled(true);
        long sws = 0l;
        for (Modul m : moduls)
            for (StudyData s : m.getStudyData()) sws += s.getSws();
        return sws;

    }

    public List<StatisticDto> getStudyTypeStatistics() {
        List<StudyType> studyTypes = studyTypeRepo.findAll();
        List<Modul> moduls = modulRepo.findByEnabled(true);
        List<StatisticDto> studyTypeStatistics = new ArrayList<>();
        for (StudyType s : studyTypes) studyTypeStatistics.add(new StatisticDto(s.getName(), 0));
        for (Modul m : moduls)
            for (StudyData s : m.getStudyData())
            for (StatisticDto st :studyTypeStatistics){
                if(st.getName().equals(s.getStudyType().getName())){
                    st.increaseCount(1);break;
                }
            }
        return studyTypeStatistics;

    }

    public List<StatisticDto> getUsersWorkloads() {
        List<Modul> moduls = modulRepo.findByEnabled(true);
        List<User> users = userRepo.findByEnabled(true);

        List<StatisticDto> usersWorkloads = new ArrayList<>();
        for(User u:users)usersWorkloads.add(new StatisticDto(u.getFullName(), 0));
        for (Modul m : moduls)
            for (User s : m.getManagers())
                for (StatisticDto st :usersWorkloads){
                    if(st.getName().equals(s.getFullName())){
                        st.increaseCount(m.getCredits()*30);break;
                    }
                }
        return usersWorkloads;
    }


}
