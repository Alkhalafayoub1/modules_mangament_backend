package modules_management.service;

import modules_management.dto.SubSubDepartmentDto;
import modules_management.entity.SubSubDepartment;
import modules_management.exception.DepartmentException;
import modules_management.repository.SubSubDepartmentRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SubSubDepartmentService {
    @Autowired
    SubSubDepartmentRepo subSubDepartmentRepo;

    public ResponseEntity<?> save(SubSubDepartmentDto subSubDepartmentDto) {
        if (subSubDepartmentDto.getName() == null || subSubDepartmentDto.getName().isEmpty())
            return new ResponseEntity<>("Department must have name at least", HttpStatus.BAD_REQUEST);
        if (subSubDepartmentRepo.findByName(subSubDepartmentDto.getName()) != null && subSubDepartmentDto.getId() == null)
            return new ResponseEntity<>("Department already exist..", HttpStatus.IM_USED);
        SubSubDepartment subSubDepartment = new SubSubDepartment(subSubDepartmentDto);
        if (subSubDepartment.getId() != null)
            log.info("\n {}\n updated to {}\n by {}", subSubDepartmentRepo.findById(subSubDepartmentDto.getId()).get(), subSubDepartment, subSubDepartmentDto);
        else log.info("\n new {}\n added by {}", subSubDepartment, subSubDepartmentDto);
        SubSubDepartment save = subSubDepartmentRepo.save(subSubDepartment);
        return new ResponseEntity<>(new SubSubDepartmentDto(Objects.requireNonNull(subSubDepartmentRepo.findById(save.getId()).orElse(null))), HttpStatus.CREATED);

    }

    public List<SubSubDepartmentDto> getAll() {


        subSubDepartmentRepo.findAll().forEach(f -> {
            System.out.println(f.toString());
        });
        return subSubDepartmentRepo.findAll().stream().map(SubSubDepartmentDto::new).collect(Collectors.toList());
    }

    public SubSubDepartmentDto getById(String id) {
        return new SubSubDepartmentDto(Objects.requireNonNull(subSubDepartmentRepo.findById(id).orElseThrow(() -> new DepartmentException("Department Not Found"))));
    }


    public void deleteById(String id) {
        subSubDepartmentRepo.deleteById(id);
    }
    public Page<SubSubDepartmentDto> getPage(Pageable pageable) {
        Page<SubSubDepartment> entities = subSubDepartmentRepo.findAll(pageable);
        Page<SubSubDepartmentDto> dtoPage = entities.map(new Function<SubSubDepartment, SubSubDepartmentDto>() {
            @Override
            public SubSubDepartmentDto apply(SubSubDepartment entity) {
                SubSubDepartmentDto dto = new SubSubDepartmentDto(entity);
                // Conversion logic
                return dto;
            }
        });
        return dtoPage;
    }

}
