package modules_management.service;

import modules_management.dto.DepartmentDto;
import modules_management.entity.Department;
import modules_management.exception.DepartmentException;
import modules_management.repository.DepartmentRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DepartmentService {


    @Autowired
    DepartmentRepo departmentRepo;

    public ResponseEntity<?> save(DepartmentDto departmentDto) {
        if (departmentDto.getName() == null || departmentDto.getName().isEmpty())
            return new ResponseEntity<>("Department must have name at least", HttpStatus.BAD_REQUEST);
        if (departmentRepo.findByName(departmentDto.getName()) != null && departmentDto.getId() == null)
            return new ResponseEntity<>("Department already exist..", HttpStatus.IM_USED);
        Department department = new Department(departmentDto);
        if (department.getId() != null)
            log.info("\n {}\n updated to {}\n by {}", departmentRepo.findById(departmentDto.getId()).get(), department, departmentDto);
        else log.info("\n new {}\n added by {}", department, departmentDto);
        Department save = departmentRepo.save(department);
        return new ResponseEntity<>(new DepartmentDto(Objects.requireNonNull(departmentRepo.findById(save.getId()).orElse(null))), HttpStatus.CREATED);

    }

    public List<DepartmentDto> getAll() {
        return departmentRepo.findAll().stream().map(DepartmentDto::new).collect(Collectors.toList());
    }

    public DepartmentDto getById(String id) {
        return new DepartmentDto(Objects.requireNonNull(departmentRepo.findById(id).orElseThrow(() -> new DepartmentException("Department Not Found"))));
    }


    public void deleteById(String id) {
        departmentRepo.deleteById(id);
    }

    public Page<DepartmentDto> getPage(Pageable pageable) {
        Page<Department> entities = departmentRepo.findAll(pageable);
        Page<DepartmentDto> dtoPage = entities.map(new Function<Department, DepartmentDto>() {
            @Override
            public DepartmentDto apply(Department entity) {
                DepartmentDto dto = new DepartmentDto(entity);
                // Conversion logic

                return dto;
            }
        });

        return dtoPage;
    }
}
