package modules_management.repository;

import modules_management.entity.Department;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DepartmentRepo extends MongoRepository<Department, String> {
    Department findByName(String name);
}
