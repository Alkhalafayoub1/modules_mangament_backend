package modules_management.repository;

import modules_management.entity.Modul;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ModulRepo extends MongoRepository<Modul, String> {
    Modul findByName(String name);
    long countByEnabled(boolean enabled);
    List<Modul> findByEnabled(boolean enabled);


}
