package modules_management.repository;

import modules_management.entity.SubDepartment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SubDepartmentRepo extends MongoRepository<SubDepartment, String> {
    SubDepartment findByName(String name);
}
