package modules_management.repository;

import modules_management.entity.SubSubDepartment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SubSubDepartmentRepo extends MongoRepository<SubSubDepartment, String> {
    SubSubDepartment findByName(String name);
}
