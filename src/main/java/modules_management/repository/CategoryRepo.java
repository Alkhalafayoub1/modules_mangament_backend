package modules_management.repository;

import modules_management.entity.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CategoryRepo extends MongoRepository<Category, String> {
    Category findByName(String name);

}
