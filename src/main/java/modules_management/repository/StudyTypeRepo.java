package modules_management.repository;

import modules_management.entity.StudyType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudyTypeRepo extends MongoRepository<StudyType, String> {
    StudyType findByName(String name);
}
