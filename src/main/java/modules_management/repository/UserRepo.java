package modules_management.repository;


import modules_management.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends MongoRepository<User, String> {

    Boolean existsByEmail(String email);

    Optional<User> findByEmail(String email);

    List<User> findByEnabled(boolean b);

    Long countByEnabled(boolean b);

}
