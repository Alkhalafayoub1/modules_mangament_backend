package modules_management.entity;

import modules_management.dto.ModulDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.*;


@Data
@NoArgsConstructor
@Document(collection = "modul")
public class Modul {
    @Id
    private String id;
    @NotBlank
    @Indexed(unique = true)
    String name;
    @Size(max = 20)
    @Indexed(unique = true)
    private int modulno;
    private SemesterType semesterType;
    @Size(max = 20)
    @Indexed(unique = true)
    private String shortcut;


    int credits;
    boolean enabled, opened;
    @DBRef
    private Department department;
    @DBRef
    private SubDepartment subDepartment;
    private List<SubSubDepData> subsubdepartments =new ArrayList<>();
    private double duration;
    private List<StudyData> studyData = new ArrayList<>();
    @DBRef
    private Category category;
    private List<Integer> semesters = new ArrayList<>();
    @DBRef
    private Set<User> managers = new HashSet<>();
    @DBRef
    private User creator;
    @DBRef
    private User updater;
    private Date updateTime=new Date();


    public Modul(ModulDto modul) {
        setId(modul.getId());
        this.name = modul.getName();
        this.shortcut = modul.getShortcut();
        this.semesterType = modul.getSemesterType();
        this.duration = modul.getDuration();
        this.studyData = modul.getStudyData();
        this.semesters = modul.getSemesters();
        this.modulno = modul.getModulno();
        this.credits = modul.getCredits();
        this.department = new Department(modul.getDepartmentDto());
if(modul.getSubDepartment()!=null&&modul.getSubDepartment().getId()!=null)this.subDepartment=new SubDepartment(modul.getSubDepartment());
        modul.getSubsubdepartments().forEach(s->this.subsubdepartments.add(new SubSubDepData(s)));
if(modul.getCategory()!=null)this.category=new Category(modul.getCategory());
if(modul.getManagers()!=null)modul.getManagers().forEach(m->this.managers.add(new User(m)));



    }


}
