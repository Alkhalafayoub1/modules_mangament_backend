package modules_management.entity;

import modules_management.dto.SubSubDepDataDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Data
@NoArgsConstructor
public class SubSubDepData {
    @DBRef
    SubSubDepartment subSubDepartment;
   ModulType modulType;

    public SubSubDepData(SubSubDepDataDto subSubDepDataDto) {
        this.subSubDepartment = new SubSubDepartment(subSubDepDataDto.getSubSubDepartment());
        this.modulType = subSubDepDataDto.getModulType();
    }
}
