package modules_management.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;


@Data
@NoArgsConstructor
@Document(collection = "modul")
public class ModulArchive {
    @Id
    private String archiveId;
    private String id;
    String name;
    private int modulno;
    private SemesterType semesterType;
    private String shortcut;
    int credits;
    boolean enabled, opened;
    @DBRef
    Department department;
    @DBRef
    SubDepartment subDepartment;
    @DBRef
    SubSubDepartment subSubDepartment;
    private double duration;
    private List<StudyData> studyData = new ArrayList<>();
    @DBRef
    private List<Category> categories = new ArrayList<>();
    private List<Integer> semesters = new ArrayList<>();
    @DBRef
    private Set<User> managers = new HashSet<>();
    @DBRef
    private User creator;
    @DBRef
    private User updater;
    private Date updateTime=new Date();

}
