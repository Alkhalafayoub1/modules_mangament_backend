package modules_management.entity;

import modules_management.dto.SubDepartmentDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "sub_departments")
public class SubDepartment extends BaseEntity {

    @DBRef
    Department department;


    public SubDepartment(SubDepartmentDto subDepartmentDto) {
        setId(subDepartmentDto.getId());
        this.name = subDepartmentDto.getName();
        if (subDepartmentDto.getDepartmentDto() != null)
            this.department = new Department(subDepartmentDto.getDepartmentDto());
    }
}
