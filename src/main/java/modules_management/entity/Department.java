package modules_management.entity;

import modules_management.dto.DepartmentDto;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@RequiredArgsConstructor
@Document(collection = "departments")
public class Department extends BaseEntity {



    public Department(DepartmentDto departmentDto) {
        if(departmentDto!=null){
        setId(departmentDto.getId());
        this.name = departmentDto.getName();}
    }




}
