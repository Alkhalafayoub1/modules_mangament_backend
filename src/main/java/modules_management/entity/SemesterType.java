package modules_management.entity;

public enum SemesterType {
    SUMMER, WINTER, ANY
}
