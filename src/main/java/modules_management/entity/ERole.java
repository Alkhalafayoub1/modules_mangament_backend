package modules_management.entity;

public enum ERole {
    ROLE_USER,
    ROLE_PROFESSOR,
    ROLE_ADMIN
}
