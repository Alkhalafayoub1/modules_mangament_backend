package modules_management.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.DBRef;


@Data
public class StudyData {
    @DBRef
    StudyType studyType;
    int sws;
    int students;
}
