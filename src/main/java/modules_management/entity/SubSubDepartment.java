package modules_management.entity;


import modules_management.dto.SubSubDepartmentDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "sub_sub_departments")
public class SubSubDepartment extends BaseEntity {

    @DBRef
    SubDepartment subDepartment;


    public SubSubDepartment(SubSubDepartmentDto subSubDepartmentDto) {
        setId(subSubDepartmentDto.getId());
        this.name = subSubDepartmentDto.getName();
        if (subSubDepartmentDto.getSubDepartmentDto() != null)
            this.subDepartment = new SubDepartment(subSubDepartmentDto.getSubDepartmentDto());

    }
}
