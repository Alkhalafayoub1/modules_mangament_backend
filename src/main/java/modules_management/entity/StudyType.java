package modules_management.entity;


import modules_management.dto.BaseDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@NoArgsConstructor
@Document(collection = "study_types")
public class StudyType extends BaseEntity {

    public StudyType(BaseDto baseDto) {
        super(baseDto);
    }
}
