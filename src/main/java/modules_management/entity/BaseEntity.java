package modules_management.entity;


import modules_management.dto.BaseDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;


@NoArgsConstructor
@Setter
@Getter
public class BaseEntity {
    @NotBlank
    @Indexed(unique = true)
    String name;
    @Id
    private String id;

    public BaseEntity(BaseDto baseDto) {
        this.id = baseDto.getId();
        this.name = baseDto.getName();
    }

    @Override
    public String toString() {
        return
                "id='" + id + '\'' +
                        ", name='" + name + '\'';
    }
}
