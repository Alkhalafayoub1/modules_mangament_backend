package modules_management.entity;


import modules_management.dto.CategoryDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@NoArgsConstructor
@Document(collection = "categories")
@Setter
@Getter
public class Category extends BaseEntity {

    @DBRef
    User manager;

    public Category(CategoryDto categoryDto) {
        this.name = categoryDto.getName();
        this.setId(categoryDto.getId());
        if (categoryDto.getManager() != null)
            this.manager = new User(categoryDto.getManager());
    }

    @Override
    public String toString() {
        return "Category{" + super.toString() +
                "manager=" + manager +
                "} ";
    }
}
