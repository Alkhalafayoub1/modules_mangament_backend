package modules_management.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@RequiredArgsConstructor
@Document(collection = "roles")
public class Role {
    @Id
    private String id;

    private ERole name;


    public Role(ERole role) {
        this.name = role;
    }
}
