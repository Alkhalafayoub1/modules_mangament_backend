package modules_management.entity;

import modules_management.dto.UserDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Document(collection = "users")
public class User {

    @Id
    private String id;
    @NotBlank
    @Size(max = 20)
    private String firstName, lastName;
    @NotBlank
    @Size(max = 50)
    @Indexed(unique = true)
    @Email
    private String email;

    @NotBlank
    @Size(max = 120)
    private String password;

    @DBRef
    private Set<Role> roles = new HashSet<>();

    private boolean enabled;
    private Date created_Time=new Date();

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(UserDto manager) {
        setId(manager.getId());
        this.lastName = manager.getLastName();
        this.firstName = manager.getFirstName();
        this.email = manager.getEmail();
        this.enabled=manager.isEnabled();

    }


    public String getFullName() {
        return getLastName() + " " + getFirstName();
    }


}
